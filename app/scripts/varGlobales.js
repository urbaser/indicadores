'use strict';

/**
 * @ngdoc service
 * @name angularApp.cargarCompetidores
 * @description
 * # cargarCompetidores
 * Service in the angularApp.
 */
angular.module('indicadoresApp')
  .constant('CONSTANTES', {
    IP: "http://intranet.urbaser.com/sitios/gestiondelconocimiento/VT/",
    IP_USUARIO: "http://intranet.urbaser.com/_api/SP.UserProfiles.PeopleManager/GetPropertiesFor(accountName=@v)?@v=%27urbaser\\nombreUsuario%27"
  });
