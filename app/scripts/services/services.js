'use strict';

/**
 * @ngdoc service
 * @name angularApp.cargarCompetidores
 * @description
 * # cargarCompetidores
 * Service in the angularApp.
 */
angular.module('indicadoresApp')
  .service('cargarIndicadoresService', ['$http', 'CONSTANTES', function ($http, CONSTANTES) {

    var mocks = false;

    var requestHeader = {
      get: {
        'headers': {
          'accept': 'application/json;odata=verbose'
        }
      }
    };

    return{
      getIndicadoresTotales: getIndicadoresTotales,
      getIndicadoresBibliotecas: getIndicadoresBibliotecas,
      getIndicadoresBibliotecas2: getIndicadoresBibliotecas2,
      getIndicadoresCreatividad: getIndicadoresCreatividad,
      getIndicadoresVt: getIndicadoresVt,
      getIndicadoresVt2: getIndicadoresVt2,
      getIndicadoresCalendario: getIndicadoresCalendario,
      getIndicadoresCalendario2: getIndicadoresCalendario2,
      getIndicadoresConocimiento: getIndicadoresConocimiento,
      getPersonas: getPersonas,
    };

    function getIndicadoresTotales(){
      var url = CONSTANTES.IP+"_api/lists/getbytitle('Indicadores totales')/items?$top=5000";
      if(mocks){
        url = "././mocks/datosTotales.json";
      }
      var promise = $http.get(url, requestHeader.get)
      .then(function (response) {
        return response.data;
      });
      return promise;
    }

    function getIndicadoresBibliotecas(){
      var url = CONSTANTES.IP+"_api/lists/getbytitle('Indicadores bibliotecas')/items?$top=5000";
      if(mocks){
        url = "././mocks/datosBibliotecas.json";
      }
      var promise = $http.get(url, requestHeader.get)
      .then(function (response) {
        return response.data;
      });
      return promise;
    }

    function getIndicadoresBibliotecas2(){
      var url = CONSTANTES.IP+"_api/lists/getbytitle('Indicadores bibliotecas2')/items?$top=5000";
      if(mocks){
        url = "././mocks/datosBibliotecas2.json";
      }
      var promise = $http.get(url, requestHeader.get)
      .then(function (response) {
        return response.data;
      });
      return promise;
    }

    function getIndicadoresCreatividad(){
      var url = CONSTANTES.IP+"_api/lists/getbytitle('Indicadores creatividad')/items?$top=5000";
      if(mocks){
        url = "././mocks/datosCreatividad.json";
      }
      var promise = $http.get(url, requestHeader.get)
      .then(function (response) {
        return response.data;
      });
      return promise;
    }

    function getIndicadoresVt(){
      var url = CONSTANTES.IP+"_api/lists/getbytitle('Indicadores boletines vt')/items?$top=5000";
      if(mocks){
        url = "././mocks/datosVt.json";
      }
      var promise = $http.get(url, requestHeader.get)
      .then(function (response) {
        return response.data;
      });
      return promise;
    }

    function getIndicadoresVt2(){
      var url = CONSTANTES.IP+"_api/lists/getbytitle('Indicadores boletines vt2')/items?$top=5000";
      if(mocks){
        url = "././mocks/datosVt2.json";
      }
      var promise = $http.get(url, requestHeader.get)
      .then(function (response) {
        return response.data;
      });
      return promise;
    }

    function getIndicadoresCalendario(){
      var url = CONSTANTES.IP+"_api/lists/getbytitle('Indicadores calendario')/items?$top=5000";
      if(mocks){
        url = "././mocks/datosCalendario.json";
      }
      var promise = $http.get(url, requestHeader.get)
      .then(function (response) {
        return response.data;
      });
      return promise;
    }

    function getIndicadoresCalendario2(){
      var url = CONSTANTES.IP+"_api/lists/getbytitle('Indicadores calendario2')/items?$top=5000";
      if(mocks){
        url = "././mocks/datosCalendario2.json";
      }
      var promise = $http.get(url, requestHeader.get)
      .then(function (response) {
        return response.data;
      });
      return promise;
    }

    function getIndicadoresConocimiento(){
      var url = CONSTANTES.IP+"_api/lists/getbytitle('Indicadores conocimiento')/items?$top=5000";
      if(mocks){
        url = "././mocks/datosConocimiento.json";
      }
      var promise = $http.get(url, requestHeader.get)
      .then(function (response) {
        return response.data;
      });
      return promise;
    }

    function getPersonas(){
      var url = CONSTANTES.IP+"_api/lists/getbytitle('Personas')/items?$top=5000&$expand=Nombre&$select=*,Nombre/Name,Nombre/Department";
      if(mocks){
        url = "././mocks/datosPersonas.json";
      }
      var promise = $http.get(url, requestHeader.get)
      .then(function (response) {
        return response.data;
      });
      return promise;
    }

  }]);
