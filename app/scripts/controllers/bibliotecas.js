'use strict';

/**
 * @ngdoc function
 * @name indicadoresApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the indicadoresApp
 */
angular.module('indicadoresApp')
  .controller('BibliotecasCtrl', function ($scope, cargarIndicadoresService, $timeout) {

    $("#home").removeClass("active");
    $("#creatividad").removeClass("active");
    $("#bibliotecas").addClass("active");
    $("#vt").removeClass("active");
    $("#calendario").removeClass("active");
    $("#conocimiento").removeClass("active");

    $scope.showSpinner = true;

    function getMasFrecuente(array){
      if(array.length == 0)
        return null;
      var modeMap = {};
      var modeMap_t = {};
      var maxEl = array[0], maxCount = 1;
      var maxEl_t = array[0], maxCount_t = 1;
      for(var i = 0; i < array.length; i++){
        if(array[i].Item){
          var el = array[i];
          var el_t = array[i].Item.Url;
          if(modeMap_t[el_t] == null)
            modeMap_t[el_t] = 1;
          else
            modeMap_t[el_t]++;

          if(modeMap_t[el_t] > maxCount){
            maxEl_t = el_t
            maxEl = el;
            maxCount_t = modeMap_t[el_t];
            maxCount = maxCount_t;
          }
        }
      }
      return [maxEl, maxCount];
    }

    function getMasFrecuente2(array){
      if(array.length == 0)
        return null;
      var modeMap = {};
      var modeMap_t = {};
      var maxEl = array[0], maxCount = 1;
      var maxEl_t = array[0], maxCount_t = 1;
      for(var i = 0; i < array.length; i++){
        if(array[i].Item){
          var el = array[i];
          var el_t = array[i].Title;
          if(modeMap_t[el_t] == null)
            modeMap_t[el_t] = 1;
          else
            modeMap_t[el_t]++;

          if(modeMap_t[el_t] > maxCount){
            maxEl_t = el_t
            maxEl = el;
            maxCount_t = modeMap_t[el_t];
            maxCount = maxCount_t;
          }
        }
      }
      return [maxEl, maxCount];
    }

    function eliminarItem(array, item){
      var arrayDevolver = [];

      for (var i = 0; i < array.length; i++) {
        if(array[i].Title != item.Title){
          arrayDevolver.push(array[i]);
        }
      }

      return arrayDevolver;
    }

    cargarIndicadoresService.getIndicadoresBibliotecas().then(function(data) {
      var datos = data.d.results;
      var hoy = new Date();
      var anioHoy = hoy.getFullYear();
      var mesHoy = hoy.getMonth();
      var haceUnAnio = new Date().setYear(hoy.getFullYear()-1);
      var haceUnAnioValor = new Date(haceUnAnio).getFullYear();
      var haceUnAnioMes = new Date(haceUnAnio).getMonth()+1;
      var meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
      var labelsUltimos12Meses = [];
      var datosUltimos12Meses_1 = [];
      var datosUltimos12Meses_2 = [];
      var datosUltimos12Meses_3 = [];
      var datosUltimos12Meses_4 = [];
      var datosMasAccedidosUltimos12Meses_1 = [];
      var datosMasAccedidosUltimos12Meses_2 = [];
      var datosMasAccedidosUltimos12Meses_3 = [];
      var dataAux_1 = [];
      var dataAux_2 = [];
      var dataAux_3 = [];
      var aux_1 = [];
      var aux_2 = [];
      var aux_3 = [];
      for (var i = 0; i < 12; i++) {
        datosUltimos12Meses_1.push(0);
        datosUltimos12Meses_2.push(0);
        datosUltimos12Meses_3.push(0);
        datosUltimos12Meses_4.push(0);
        datosMasAccedidosUltimos12Meses_1.push(0);
        datosMasAccedidosUltimos12Meses_2.push(0);
        datosMasAccedidosUltimos12Meses_3.push(0);
        dataAux_1.push("");
        dataAux_2.push("");
        dataAux_3.push("");
        aux_1.push([]);
        aux_2.push([]);
        aux_3.push([]);
      }

      //LABELS
      for (var i = haceUnAnioValor; i <= anioHoy; i++) {
        if (i == haceUnAnioValor) {
          for(var j = haceUnAnioMes; j<12; j++){
            labelsUltimos12Meses.push(meses[j]+" "+i);
          }
        }else{
          for(var j = 0; j<=mesHoy; j++){
            labelsUltimos12Meses.push(meses[j]+" "+i);
          }
        }
      }

      //DATOS
      var anio;
      var mes;
      var fecha;
      var dato;
      var index;
      var fechaComparar;
      for (var i = 0; i < datos.length; i++) {
        fecha = new Date(datos[i].Fecha);
        anio = fecha.getFullYear();
        mes = fecha.getMonth();
        fechaComparar = meses[mes]+" "+anio;
        dato = datos[i];

        if(fecha>=haceUnAnio && fecha<=hoy){
          //Datos ultimos 12 meses
          if(haceUnAnioValor == anioHoy){
            if(dato.Title.indexOf("Documentación de interés")!="-1"){
              datosUltimos12Meses_1[mes]++;
            }else if (dato.Title.indexOf("Patentes")!="-1") {
              datosUltimos12Meses_2[mes]++;
            }else if (dato.Title.indexOf("Procedimientos")!="-1") {
              datosUltimos12Meses_3[mes]++;
            }
            datosUltimos12Meses_4[mes]=datosUltimos12Meses_1[mes]+datosUltimos12Meses_2[mes]
            +datosUltimos12Meses_3[mes];
          }else if(anio == haceUnAnioValor){
            if(dato.Title.indexOf("Documentación de interés")!="-1"){
              datosUltimos12Meses_1[mes-haceUnAnioMes]++;
            }else if (dato.Title.indexOf("Patentes")!="-1") {
              datosUltimos12Meses_2[mes-haceUnAnioMes]++;
            }else if (dato.Title.indexOf("Procedimientos")!="-1") {
              datosUltimos12Meses_3[mes-haceUnAnioMes]++;
            }
            datosUltimos12Meses_4[mes-haceUnAnioMes]=datosUltimos12Meses_1[mes-haceUnAnioMes]+datosUltimos12Meses_2[mes-haceUnAnioMes]
            +datosUltimos12Meses_3[mes-haceUnAnioMes];
          }else{
            if(dato.Title.indexOf("Documentación de interés")!="-1"){
              datosUltimos12Meses_1[11-(mesHoy-mes)]++;
            }else if (dato.Title.indexOf("Patentes")!="-1") {
              datosUltimos12Meses_2[11-(mesHoy-mes)]++;
            }else if (dato.Title.indexOf("Procedimientos")!="-1") {
              datosUltimos12Meses_3[11-(mesHoy-mes)]++;
            }
            datosUltimos12Meses_4[11-(mesHoy-mes)]=datosUltimos12Meses_1[11-(mesHoy-mes)]+datosUltimos12Meses_2[11-(mesHoy-mes)]
            +datosUltimos12Meses_3[11-(mesHoy-mes)];
          }

          //Datos mas accedidos ultimos 12 meses
          for (var j = 0; j < labelsUltimos12Meses.length; j++) {
            if(fechaComparar == labelsUltimos12Meses[j]){
              if(haceUnAnioValor == anioHoy){
                if(dato.Title.indexOf("Documentación de interés")!="-1"){
                  aux_1[mes].push(dato);
                }else if (dato.Title.indexOf("Patentes")!="-1") {
                  aux_2[mes].push(dato);
                }else if (dato.Title.indexOf("Procedimientos")!="-1") {
                  aux_3[mes].push(dato);
                }
              }else if(anio == haceUnAnioValor){
                if(dato.Title.indexOf("Documentación de interés")!="-1"){
                  aux_1[mes-haceUnAnioMes].push(dato);
                }else if (dato.Title.indexOf("Patentes")!="-1") {
                  aux_2[mes-haceUnAnioMes].push(dato);
                }else if (dato.Title.indexOf("Procedimientos")!="-1") {
                  aux_3[mes-haceUnAnioMes].push(dato);
                }
              }else{
                if(dato.Title.indexOf("Documentación de interés")!="-1"){
                  aux_1[11-(mesHoy-mes)].push(dato);
                }else if (dato.Title.indexOf("Patentes")!="-1") {
                  aux_2[11-(mesHoy-mes)].push(dato);
                }else if (dato.Title.indexOf("Procedimientos")!="-1") {
                  aux_3[11-(mesHoy-mes)].push(dato);
                }
              }
            }
          }
        }
      }

      for (var i = 0; i < 12; i++) {
        if(aux_1[i] != null){
          var elemMasFrecuente = getMasFrecuente(aux_1[i]);
          if(elemMasFrecuente!=null){
            dataAux_1[i] = elemMasFrecuente[0];
            datosMasAccedidosUltimos12Meses_1[i] = elemMasFrecuente[1];
          }
        }
        if(aux_2[i] != null){
          var elemMasFrecuente = getMasFrecuente(aux_2[i]);
          if(elemMasFrecuente!=null){
            dataAux_2[i] = elemMasFrecuente[0];
            datosMasAccedidosUltimos12Meses_2[i] = elemMasFrecuente[1];
          }
        }
        if(aux_3[i] != null){
          var elemMasFrecuente = getMasFrecuente(aux_3[i]);
          if(elemMasFrecuente!=null){
            dataAux_3[i] = elemMasFrecuente[0];
            datosMasAccedidosUltimos12Meses_3[i] = elemMasFrecuente[1];
          }
        }
      }

      $scope.azul = ["#6666FF"];
      $scope.colores = ["#66FF66", "#FF6666", "#6666FF", "#bf8040"];

      $scope.labels = labelsUltimos12Meses;
      $scope.series = ['Documentación interés', 'Patentes', 'Procedimientos', 'Bibliotecas'];

      $scope.data = [datosUltimos12Meses_1, datosUltimos12Meses_2, datosUltimos12Meses_3, datosUltimos12Meses_4];
      $scope.options = {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero:true
            }
          }]
        },
        legend: {
          display: true
        },
        tooltips: {
          callbacks: {
            label: function(tooltipItem, data) {
              var index = tooltipItem.datasetIndex;
              if(index == 0){
                var num = data.datasets[index].data[tooltipItem.index];
                var title = dataAux_1[tooltipItem.index].Title;
                if(num>0){
                  return " "+num+": "+title;
                }
              }else if(index == 1){
                var num = data.datasets[index].data[tooltipItem.index];
                var title = dataAux_2[tooltipItem.index].Title;
                if(num>0){
                  return " "+num+": "+title;
                }
              }else if(index == 2){
                var num = data.datasets[index].data[tooltipItem.index];
                var title = dataAux_3[tooltipItem.index].Title;
                if(num>0){
                  return " "+num+": "+title;
                }
              }else{
                var num = data.datasets[index].data[tooltipItem.index];
                return  " "+num+": Bibliotecas totales";
              }
            }
          }
        }
      };

      $scope.showSpinner = false;
      $timeout(function() {
        $scope.$apply();
      }, 100);

    });

    cargarIndicadoresService.getIndicadoresBibliotecas2().then(function(data) {
      var datos = data.d.results;
      var datosAux = datos.slice();
      var DiezElemMasAccedidos = [];
      var DiezElemMasAccedidosCantidad = [];

      for (var i = 0; i < 10; i++) {
        DiezElemMasAccedidos.push(getMasFrecuente2(datosAux)[0]);
        DiezElemMasAccedidosCantidad.push(getMasFrecuente2(datosAux)[1]);
        datosAux = eliminarItem(datosAux, DiezElemMasAccedidos[i]);
      }

      $scope.series2 = ['Clasificación elementos más accedidos'];
      $scope.labels2 = ["1º", "2º", "3º", "4º", "5º", "6º", "7º", "8º", "9º", "10º"];
      $scope.data2 = [DiezElemMasAccedidosCantidad];
      $scope.options2 = {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero:true
            }
          }]
        },
        legend: {
          display: true
        },
        tooltips: {
          callbacks: {
            label: function(tooltipItem, data) {
              var index = tooltipItem.index;
              var title = DiezElemMasAccedidos[index].Title;
              return title;
            }
          }
        }
      };

      $scope.showSpinner = false;
      $timeout(function() {
        $scope.$apply();
      }, 100);

    });
  });
