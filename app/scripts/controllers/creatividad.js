'use strict';

/**
 * @ngdoc function
 * @name indicadoresApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the indicadoresApp
 */
angular.module('indicadoresApp')
  .controller('CreatividadCtrl', function ($scope, cargarIndicadoresService, $timeout) {

    $("#home").removeClass("active");
    $("#creatividad").addClass("active");
    $("#bibliotecas").removeClass("active");
    $("#vt").removeClass("active");
    $("#calendario").removeClass("active");
    $("#conocimiento").removeClass("active");

    $scope.showSpinner = true;

    function getMasFrecuente(array){
      if(array.length == 0)
        return null;
      var modeMap = {};
      var modeMap_t = {};
      var maxEl = array[0], maxCount = 1;
      var maxEl_t = array[0], maxCount_t = 1;
      for(var i = 0; i < array.length; i++){
        var el = array[i];
        var el_t = array[i].Item.Url;
        if(modeMap_t[el_t] == null)
          modeMap_t[el_t] = 1;
        else
          modeMap_t[el_t]++;

        if(modeMap_t[el_t] > maxCount){
          maxEl_t = el_t
          maxEl = el;
          maxCount_t = modeMap_t[el_t];
          maxCount = maxCount_t;
        }
      }
      return [maxEl, maxCount];
    }

    cargarIndicadoresService.getIndicadoresCreatividad().then(function(data) {
      var datos = data.d.results;
      var hoy = new Date();
      var anioHoy = hoy.getFullYear();
      var mesHoy = hoy.getMonth();
      var haceUnAnio = new Date().setYear(hoy.getFullYear()-1);
      var haceUnAnioValor = new Date(haceUnAnio).getFullYear();
      var haceUnAnioMes = new Date(haceUnAnio).getMonth()+1;
      var meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
      var labelsUltimos12Meses = [];
      var datosUltimos12Meses_1 = [];
      var datosUltimos12Meses_2 = [];
      var datosUltimos12Meses_3 = [];
      var datosUltimos12Meses_4 = [];
      var datosUltimos12Meses_5 = [];
      var datosUltimos12Meses_6 = [];
      var datosMasAccedidosUltimos12Meses_1 = [];
      var datosMasAccedidosUltimos12Meses_2 = [];
      var datosMasAccedidosUltimos12Meses_3 = [];
      var datosMasAccedidosUltimos12Meses_4 = [];
      var datosMasAccedidosUltimos12Meses_5 = [];
      var datosMasAccedidosUltimos12Meses_6 = [];
      var dataAux_1 = [];
      var dataAux_2 = [];
      var dataAux_3 = [];
      var dataAux_4 = [];
      var dataAux_5 = [];
      var dataAux_6 = [];
      var aux_1 = [];
      var aux_2 = [];
      var aux_3 = [];
      var aux_4 = [];
      var aux_5 = [];
      for (var i = 0; i < 12; i++) {
        datosUltimos12Meses_1.push(0);
        datosUltimos12Meses_2.push(0);
        datosUltimos12Meses_3.push(0);
        datosUltimos12Meses_4.push(0);
        datosUltimos12Meses_5.push(0);
        datosUltimos12Meses_6.push(0);
        datosMasAccedidosUltimos12Meses_1.push(0);
        datosMasAccedidosUltimos12Meses_2.push(0);
        datosMasAccedidosUltimos12Meses_3.push(0);
        datosMasAccedidosUltimos12Meses_4.push(0);
        datosMasAccedidosUltimos12Meses_5.push(0);
        datosMasAccedidosUltimos12Meses_6.push(0);
        dataAux_1.push("");
        dataAux_2.push("");
        dataAux_3.push("");
        dataAux_4.push("");
        dataAux_5.push("");
        dataAux_6.push("");
        aux_1.push([]);
        aux_2.push([]);
        aux_3.push([]);
        aux_4.push([]);
        aux_5.push([]);
      }

      //LABELS
      for (var i = haceUnAnioValor; i <= anioHoy; i++) {
        if (i == haceUnAnioValor) {
          for(var j = haceUnAnioMes; j<12; j++){
            labelsUltimos12Meses.push(meses[j]+" "+i);
          }
        }else{
          for(var j = 0; j<=mesHoy; j++){
            labelsUltimos12Meses.push(meses[j]+" "+i);
          }
        }
      }

      //DATOS
      var anio;
      var mes;
      var fecha;
      var dato;
      var index;
      var fechaComparar;
      for (var i = 0; i < datos.length; i++) {
        fecha = new Date(datos[i].Fecha);
        anio = fecha.getFullYear();
        mes = fecha.getMonth();
        fechaComparar = meses[mes]+" "+anio;
        dato = datos[i];

        if(fecha>=haceUnAnio && fecha<=hoy){
          //Datos ultimos 12 meses
          if(haceUnAnioValor == anioHoy){
            if(dato.Title.indexOf("Vehículo eléctrico")!="-1"){
              datosUltimos12Meses_1[mes]++;
            }else if (dato.Title.indexOf("Recogida")!="-1") {
              datosUltimos12Meses_2[mes]++;
            }else if(dato.Title.indexOf("GSLixiviados")!="-1"){
              datosUltimos12Meses_3[mes]++;
            }else if (dato.Title.indexOf("Eficiencia energética")!="-1") {
              datosUltimos12Meses_4[mes]++;
            }else if(dato.Title.indexOf("galería campañas")!="-1"){
              datosUltimos12Meses_5[mes]++;
            }
            datosUltimos12Meses_6[mes]=datosUltimos12Meses_1[mes]+datosUltimos12Meses_2[mes]
            +datosUltimos12Meses_3[mes]+datosUltimos12Meses_4[mes]+datosUltimos12Meses_5[mes];
          }else if(anio == haceUnAnioValor){
            if(dato.Title.indexOf("Vehículo eléctrico")!="-1"){
              datosUltimos12Meses_1[mes-haceUnAnioMes]++;
            }else if (dato.Title.indexOf("Recogida")!="-1") {
              datosUltimos12Meses_2[mes-haceUnAnioMes]++;
            }else if(dato.Title.indexOf("Lixiviados")!="-1"){
              datosUltimos12Meses_3[mes-haceUnAnioMes]++;
            }else if (dato.Title.indexOf("Eficiencia energética")!="-1") {
              datosUltimos12Meses_4[mes-haceUnAnioMes]++;
            }else if(dato.Title.indexOf("galería campañas")!="-1"){
              datosUltimos12Meses_5[mes-haceUnAnioMes]++;
            }
            datosUltimos12Meses_6[mes-haceUnAnioMes]=datosUltimos12Meses_1[mes-haceUnAnioMes]+datosUltimos12Meses_2[mes-haceUnAnioMes]
            +datosUltimos12Meses_3[mes-haceUnAnioMes]+datosUltimos12Meses_4[mes-haceUnAnioMes]+datosUltimos12Meses_5[mes-haceUnAnioMes];
          }else{
            if(dato.Title.indexOf("Vehículo eléctrico")!="-1"){
              datosUltimos12Meses_1[11-(mesHoy-mes)]++;
            }else if (dato.Title.indexOf("Recogida")!="-1") {
              datosUltimos12Meses_2[11-(mesHoy-mes)]++;
            }else if(dato.Title.indexOf("Lixiviados")!="-1"){
              datosUltimos12Meses_3[11-(mesHoy-mes)]++;
            }else if (dato.Title.indexOf("Eficiencia energética")!="-1") {
              datosUltimos12Meses_4[11-(mesHoy-mes)]++;
            }else if(dato.Title.indexOf("galería campañas")!="-1"){
              datosUltimos12Meses_5[11-(mesHoy-mes)]++;
            }
            datosUltimos12Meses_6[11-(mesHoy-mes)]=datosUltimos12Meses_1[11-(mesHoy-mes)]+datosUltimos12Meses_2[11-(mesHoy-mes)]
            +datosUltimos12Meses_3[11-(mesHoy-mes)]+datosUltimos12Meses_4[11-(mesHoy-mes)]+datosUltimos12Meses_5[11-(mesHoy-mes)];
          }

          //Datos mas accedidos ultimos 12 meses
          for (var j = 0; j < labelsUltimos12Meses.length; j++) {
            if(fechaComparar == labelsUltimos12Meses[j]){
              if(haceUnAnioValor == anioHoy){
                if(dato.Title.indexOf("Vehículo eléctrico")!="-1"){
                  aux_1[mes].push(dato);
                }else if (dato.Title.indexOf("Recogida")!="-1") {
                  aux_2[mes].push(dato);
                }else if(dato.Title.indexOf("Lixiviados")!="-1"){
                  aux_3[mes].push(dato);
                }else if (dato.Title.indexOf("Eficiencia energética")!="-1") {
                  aux_4[mes].push(dato);
                }else if(dato.Title.indexOf("galería campañas")!="-1"){
                  aux_5[mes].push(dato);
                }
              }else if(anio == haceUnAnioValor){
                if(dato.Title.indexOf("Vehículo eléctrico")!="-1"){
                  aux_1[mes-haceUnAnioMes].push(dato);
                }else if (dato.Title.indexOf("Recogida")!="-1") {
                  aux_2[mes-haceUnAnioMes].push(dato);
                }else if(dato.Title.indexOf("Lixiviados")!="-1"){
                  aux_3[mes-haceUnAnioMes].push(dato);
                }else if (dato.Title.indexOf("Eficiencia energética")!="-1") {
                  aux_4[mes-haceUnAnioMes].push(dato);
                }else if(dato.Title.indexOf("galería campañas")!="-1"){
                  aux_5[mes-haceUnAnioMes].push(dato);
                }
              }else{
                if(dato.Title.indexOf("Vehículo eléctrico")!="-1"){
                  aux_1[11-(mesHoy-mes)].push(dato);
                }else if (dato.Title.indexOf("Recogida")!="-1") {
                  aux_2[11-(mesHoy-mes)].push(dato);
                }else if(dato.Title.indexOf("Lixiviados")!="-1"){
                  aux_3[11-(mesHoy-mes)].push(dato);
                }else if (dato.Title.indexOf("Eficiencia energética")!="-1") {
                  aux_4[11-(mesHoy-mes)].push(dato);
                }else if(dato.Title.indexOf("galería campañas")!="-1"){
                  aux_5[11-(mesHoy-mes)].push(dato);
                }
              }
            }
          }
        }
      }

      for (var i = 0; i < 12; i++) {
        if(aux_1[i] != null){
          var elemMasFrecuente = getMasFrecuente(aux_1[i]);
          if(elemMasFrecuente!=null){
            dataAux_1[i] = elemMasFrecuente[0];
            datosMasAccedidosUltimos12Meses_1[i] = elemMasFrecuente[1];
          }
        }
        if(aux_2[i] != null){
          var elemMasFrecuente = getMasFrecuente(aux_2[i]);
          if(elemMasFrecuente!=null){
            dataAux_2[i] = elemMasFrecuente[0];
            datosMasAccedidosUltimos12Meses_2[i] = elemMasFrecuente[1];
          }
        }
        if(aux_3[i] != null){
          var elemMasFrecuente = getMasFrecuente(aux_3[i]);
          if(elemMasFrecuente!=null){
            dataAux_3[i] = elemMasFrecuente[0];
            datosMasAccedidosUltimos12Meses_3[i] = elemMasFrecuente[1];
          }
        }
        if(aux_4[i] != null){
          var elemMasFrecuente = getMasFrecuente(aux_4[i]);
          if(elemMasFrecuente!=null){
            dataAux_4[i] = elemMasFrecuente[0];
            datosMasAccedidosUltimos12Meses_4[i] = elemMasFrecuente[1];
          }
        }
        if(aux_5[i] != null){
          var elemMasFrecuente = getMasFrecuente(aux_5[i]);
          if(elemMasFrecuente!=null){
            dataAux_5[i] = elemMasFrecuente[0];
            datosMasAccedidosUltimos12Meses_5[i] = elemMasFrecuente[1];
          }
        }
      }

      $scope.verde = ["#00FF00"];
      $scope.colores = ["#66FF66", "#FF6666", "#6666FF", "#ff66ff", "#ffb84d", "#bf8040"];

      $scope.labels = labelsUltimos12Meses;
      $scope.series = ['Vehículo eléctrico', 'Recogida', 'Lixiviados', 'Eficiencia energética', 'Galería', 'Creatividad'];

      $scope.data = [datosUltimos12Meses_1, datosUltimos12Meses_2, datosUltimos12Meses_3, datosUltimos12Meses_4,
        datosUltimos12Meses_5, datosUltimos12Meses_6];
      $scope.options = {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero:true
            }
          }]
        },
        legend: {
          display: true
        },
        tooltips: {
          callbacks: {
            label: function(tooltipItem, data) {
              var index = tooltipItem.datasetIndex;
              if(index == 0){
                var num = data.datasets[index].data[tooltipItem.index];
                var elem = datosMasAccedidosUltimos12Meses_1[tooltipItem.index];
                var title = dataAux_1[tooltipItem.index].Title;
                if(elem>0){
                  return " "+num+": "+title;
                }
              }else if(index == 1){
                var num = data.datasets[index].data[tooltipItem.index];
                var elem = datosMasAccedidosUltimos12Meses_2[tooltipItem.index];
                var title = dataAux_2[tooltipItem.index].Title;
                if(elem>0){
                  return " "+num+": "+title;
                }
              }else if(index == 2){
                var num = data.datasets[index].data[tooltipItem.index];
                var elem = datosMasAccedidosUltimos12Meses_3[tooltipItem.index];
                var title = dataAux_3[tooltipItem.index].Title;
                if(elem>0){
                  return " "+num+": "+title;
                }
              }else if(index == 3){
                var num = data.datasets[index].data[tooltipItem.index];
                var elem = datosMasAccedidosUltimos12Meses_4[tooltipItem.index];
                var title = dataAux_4[tooltipItem.index].Title;
                if(elem>0){
                  return " "+num+": "+title;
                }
              }else if(index == 4){
                var num = data.datasets[index].data[tooltipItem.index];
                var elem = datosMasAccedidosUltimos12Meses_5[tooltipItem.index];
                var title = dataAux_5[tooltipItem.index].Title;
                if(elem>0){
                  return " "+num+": "+title;
                }
              }else{
                var num = data.datasets[index].data[tooltipItem.index];
                return  " "+num+": Creatividad";
              }
            }
          }
        }
      };

      $scope.data2 = [datosMasAccedidosUltimos12Meses_1, datosMasAccedidosUltimos12Meses_2, datosMasAccedidosUltimos12Meses_3,
        datosMasAccedidosUltimos12Meses_4, datosMasAccedidosUltimos12Meses_5];
      $scope.options2 = {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero:true
            }
          }]
        },
        legend: {
          display: true
        },
        tooltips: {
          callbacks: {
            label: function(tooltipItem, data) {
              var index = tooltipItem.datasetIndex;
              if(index == 0){
                var num = data.datasets[index].data[tooltipItem.index];
                var elem = dataAux_1[tooltipItem.index];
                if(elem){
                  var title = elem.Title;
                  if(title.length>85){
                    return " "+num+": ..."+title.substring(title.length/2, title.length-1);
                  }else{
                    return " "+num+": "+title;
                  }
                }
              }else if(index == 1){
                var num = data.datasets[index].data[tooltipItem.index];
                var elem = dataAux_2[tooltipItem.index];
                if(elem){
                  var title = elem.Title;
                  if(title.length>85){
                    return " "+num+": ..."+title.substring(title.length/2, title.length-1);
                  }else{
                    return " "+num+": "+title;
                  }
                }
              }else if(index == 2){
                var num = data.datasets[index].data[tooltipItem.index];
                var elem = dataAux_3[tooltipItem.index];
                if(elem){
                  var title = elem.Title;
                  if(title.length>85){
                    return " "+num+": ..."+title.substring(title.length/2, title.length-1);
                  }else{
                    return " "+num+": "+title;
                  }
                }
              }else if(index == 3){
                var num = data.datasets[index].data[tooltipItem.index];
                var elem = dataAux_4[tooltipItem.index];
                if(elem){
                  var title = elem.Title;
                  if(title.length>85){
                    return " "+num+": ..."+title.substring(title.length/2, title.length-1);
                  }else{
                    return " "+num+": "+title;
                  }
                }
              }else if(index == 4){
                var num = data.datasets[index].data[tooltipItem.index];
                var elem = dataAux_5[tooltipItem.index];
                if(elem){
                  var title = elem.Title;
                  if(title.length>85){
                    return " "+num+": ..."+title.substring(title.length/2, title.length-1);
                  }else{
                    return " "+num+": "+title;
                  }
                }
              }
            }
          }
        }
      };

      $scope.showSpinner = false;
      $timeout(function() {
        $scope.$apply();
      }, 100);

    });

  });
