'use strict';

/**
 * @ngdoc function
 * @name indicadoresApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the indicadoresApp
 */
angular.module('indicadoresApp')
  .controller('CalendarioCtrl', function ($scope, cargarIndicadoresService, $timeout) {

    $("#home").removeClass("active");
    $("#creatividad").removeClass("active");
    $("#bibliotecas").removeClass("active");
    $("#vt").removeClass("active");
    $("#calendario").addClass("active");
    $("#conocimiento").removeClass("active");

    $scope.showSpinner = true;

    function getMasFrecuente(array){
      if(array.length == 0)
        return null;
      var modeMap = {};
      var modeMap_t = {};
      var maxEl = array[0], maxCount = 1;
      var maxEl_t = array[0], maxCount_t = 1;
      for(var i = 0; i < array.length; i++){
        var el = array[i];
        var el_t = array[i].Item.Url;
        if(modeMap_t[el_t] == null)
          modeMap_t[el_t] = 1;
        else
          modeMap_t[el_t]++;

        if(modeMap_t[el_t] > maxCount){
          maxEl_t = el_t
          maxEl = el;
          maxCount_t = modeMap_t[el_t];
          maxCount = maxCount_t;
        }
      }
      return [maxEl, maxCount];
    }

    function getMasFrecuente2(array){
      if(array.length == 0)
        return null;
      var modeMap = {};
      var modeMap_t = {};
      var maxEl = array[0], maxCount = 1;
      var maxEl_t = array[0], maxCount_t = 1;
      for(var i = 0; i < array.length; i++){
        if(array[i].Item){
          var el = array[i];
          var el_t = array[i].Title;
          if(modeMap_t[el_t] == null)
            modeMap_t[el_t] = 1;
          else
            modeMap_t[el_t]++;

          if(modeMap_t[el_t] > maxCount){
            maxEl_t = el_t
            maxEl = el;
            maxCount_t = modeMap_t[el_t];
            maxCount = maxCount_t;
          }
        }
      }
      return [maxEl, maxCount];
    }

    function eliminarItem(array, item){
      var arrayDevolver = [];

      for (var i = 0; i < array.length; i++) {
        if(array[i].Title != item.Title){
          arrayDevolver.push(array[i]);
        }
      }

      return arrayDevolver;
    }

    cargarIndicadoresService.getIndicadoresCalendario().then(function(data) {
      var datos = data.d.results;
      var hoy = new Date();
      var anioHoy = hoy.getFullYear();
      var mesHoy = hoy.getMonth();
      var haceUnAnio = new Date().setYear(hoy.getFullYear()-1);
      var haceUnAnioValor = new Date(haceUnAnio).getFullYear();
      var haceUnAnioMes = new Date(haceUnAnio).getMonth()+1;
      var meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
      var labelsUltimos12Meses = [];
      var datosUltimos12Meses = [];
      for (var i = 0; i < 12; i++) {
        datosUltimos12Meses.push(0);
      }

      //LABELS
      for (var i = haceUnAnioValor; i <= anioHoy; i++) {
        if (i == haceUnAnioValor) {
          for(var j = haceUnAnioMes; j<12; j++){
            labelsUltimos12Meses.push(meses[j]+" "+i);
          }
        }else{
          for(var j = 0; j<=mesHoy; j++){
            labelsUltimos12Meses.push(meses[j]+" "+i);
          }
        }
      }

      //DATOS
      var anio;
      var mes;
      var fecha;
      var dato;
      var index;
      var fechaComparar;
      for (var i = 0; i < datos.length; i++) {
        fecha = new Date(datos[i].Fecha);
        anio = fecha.getFullYear();
        mes = fecha.getMonth();
        fechaComparar = meses[mes]+" "+anio;
        dato = datos[i];

        if(fecha>=haceUnAnio && fecha<=hoy){
          //Datos ultimos 12 meses
          if(haceUnAnioValor == anioHoy){
            datosUltimos12Meses[mes]++;
          }else if(anio == haceUnAnioValor){
            datosUltimos12Meses[mes-haceUnAnioMes]++;
          }else{
            datosUltimos12Meses[11-(mesHoy-mes)]++;
          }
        }
      }

      $scope.verde = ["#00FF00"];
      $scope.marron = ["#bf8040"];

      $scope.data = [datosUltimos12Meses];
      $scope.labels = labelsUltimos12Meses;
      $scope.options = {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero:true
            }
          }]
        }
      };

      $scope.showSpinner = false;
      $timeout(function() {
        $scope.$apply();
      }, 100);

    });

    cargarIndicadoresService.getIndicadoresCalendario2().then(function(data) {
      var datos = data.d.results;
      var datosAux = datos.slice();
      var DiezElemMasAccedidos = [];
      var DiezElemMasAccedidosCantidad = [];

      for (var i = 0; i < 10; i++) {
        DiezElemMasAccedidos.push(getMasFrecuente2(datosAux)[0]);
        DiezElemMasAccedidosCantidad.push(getMasFrecuente2(datosAux)[1]);
        datosAux = eliminarItem(datosAux, DiezElemMasAccedidos[i]);
      }

      $scope.series2 = ['Clasificación elementos más accedidos'];
      $scope.labels2 = ["1º", "2º", "3º", "4º", "5º", "6º", "7º", "8º", "9º", "10º"];
      $scope.data2 = [DiezElemMasAccedidosCantidad];
      $scope.options2 = {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero:true
            }
          }]
        },
        legend: {
          display: true
        },
        tooltips: {
          callbacks: {
            label: function(tooltipItem, data) {
              var index = tooltipItem.index;
              var title = DiezElemMasAccedidos[index].Title;
              return title;
            }
          }
        }
      };

      $scope.showSpinner = false;
      $timeout(function() {
        $scope.$apply();
      }, 100);

    });
  });
