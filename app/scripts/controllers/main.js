'use strict';

/**
 * @ngdoc function
 * @name indicadoresApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the indicadoresApp
 */
angular.module('indicadoresApp')
  .controller('MainCtrl', function ($scope, cargarIndicadoresService, $timeout) {

    $("#home").addClass("active");
    $("#creatividad").removeClass("active");
    $("#bibliotecas").removeClass("active");
    $("#vt").removeClass("active");
    $("#calendario").removeClass("active");
    $("#conocimiento").removeClass("active");

    $scope.showSpinner = true;
    $scope.showSpinner2 = true;

    var customOrder = function(a, b){
      if(a[2]<b[2]){
        return 1;
      }else if(a[2]>b[2]){
        return -1;
      }else{
        return 0;
      }
    }

    function getMasFrecuente(array){
      if(array.length == 0)
        return null;
      var modeMap = {};
      var maxEl = array[0], maxCount = 1;
      for(var i = 0; i < array.length; i++){
        var el = array[i];
        if(modeMap[el] == null)
          modeMap[el] = 1;
        else
          modeMap[el]++;

        if(modeMap[el] > maxCount){
          maxEl = el;
          maxCount = modeMap[el];
        }
      }
      return [maxEl, maxCount];
    }

    cargarIndicadoresService.getIndicadoresTotales().then(function(data) {
      var datos = data.d.results;
      var hoy = new Date();
      var anioHoy = hoy.getFullYear();
      var mesHoy = hoy.getMonth();
      var haceUnAnio = new Date().setYear(hoy.getFullYear()-1);
      var haceUnAnioValor = new Date(haceUnAnio).getFullYear();
      var haceUnAnioMes = new Date(haceUnAnio).getMonth()+1;
      var meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
      var labelsUltimos12Meses = [];
      var datosMasAccedidosUltimos12Meses_1 = [];
      var datosMasAccedidosUltimos12Meses_2 = [];
      var datosMasAccedidosUltimos12Meses_3 = [];
      var datosMasAccedidosUltimos12Meses_4 = [];
      var datosMasAccedidosUltimos12Meses_5 = [];
      var datosMasAccedidosUltimos12Meses_6 = [];
      var datosMasAccedidosUltimos12Meses_7 = [];
      var datosMasAccedidosUltimos12Meses_8 = [];
      var dataAux_1 = [];
      var dataAux_2 = [];
      var dataAux_3 = [];
      var dataAux_4 = [];
      var dataAux_5 = [];
      var dataAux_6 = [];
      var dataAux_7 = [];
      var aux_1 = [];
      var aux_2 = [];
      var aux_3 = [];
      var aux_4 = [];
      var aux_5 = [];
      var aux_6 = [];
      var aux_7 = [];

      for (var i = 0; i < 12; i++) {
        datosMasAccedidosUltimos12Meses_1.push(0);
        datosMasAccedidosUltimos12Meses_2.push(0);
        datosMasAccedidosUltimos12Meses_3.push(0);
        datosMasAccedidosUltimos12Meses_4.push(0);
        datosMasAccedidosUltimos12Meses_5.push(0);
        datosMasAccedidosUltimos12Meses_6.push(0);
        datosMasAccedidosUltimos12Meses_7.push(0);
        datosMasAccedidosUltimos12Meses_8.push(0);
        dataAux_1.push("");
        dataAux_2.push("");
        dataAux_3.push("");
        dataAux_4.push("");
        dataAux_5.push("");
        dataAux_6.push("");
        dataAux_7.push("");
        aux_1.push([]);
        aux_2.push([]);
        aux_3.push([]);
        aux_4.push([]);
        aux_5.push([]);
        aux_6.push([]);
        aux_7.push([]);
      }

      //LABELS
      for (var i = haceUnAnioValor; i <= anioHoy; i++) {
        if (i == haceUnAnioValor) {
          for(var j = haceUnAnioMes; j<12; j++){
            labelsUltimos12Meses.push(meses[j]+" "+i);
          }
        }else{
          for(var j = 0; j<=mesHoy; j++){
            labelsUltimos12Meses.push(meses[j]+" "+i);
          }
        }
      }

      //DATOS
      var anio;
      var mes;
      var fecha;
      var dato;
      var title;
      var index;
      var fechaComparar;
      var fechaMasRecienteAux;
      var fechaMasReciente = "";
      for (var i = 0; i < datos.length; i++) {
        fecha = new Date(datos[i].Fecha);
        fechaMasRecienteAux = new Date(datos[i].Created);
        fechaMasReciente = new Date(datos[i].Fecha);
        anio = fecha.getFullYear();
        mes = fecha.getMonth();
        fechaComparar = meses[mes]+" "+anio;
        dato = datos[i];

        if(fechaMasReciente == "" || fechaMasRecienteAux > fechaMasReciente){
          fechaMasReciente = fechaMasRecienteAux;
        }

        if(fecha>=haceUnAnio && fecha<=hoy){
          //Datos mas accedidos ultimos 12 meses
          for (var j = 0; j < labelsUltimos12Meses.length; j++) {
            if(fechaComparar == labelsUltimos12Meses[j]){
              if(haceUnAnioValor == anioHoy){
                if(dato.Title.indexOf("Creatividad")!="-1"){
                  aux_1[mes].push(dato);
                }else if (dato.Title.indexOf("Biblioteca")!="-1") {
                  aux_2[mes].push(dato);
                }else if (dato.Title.indexOf("VT")!="-1") {
                  aux_3[mes].push(dato);
                }else if (dato.Title.indexOf("calendario eventos")!="-1") {
                  aux_4[mes].push(dato);
                }else if (dato.Title.indexOf("Generando conocimiento")!="-1") {
                  aux_5[mes].push(dato);
                }else if (dato.Title.indexOf("Portada")!="-1") {
                  aux_6[mes].push(dato);
                }else if (dato.Title.indexOf("informe vtgc")!="-1") {
                  aux_7[mes].push(dato);
                }
              }else if(anio == haceUnAnioValor){
                if(dato.Title.indexOf("Creatividad")!="-1"){
                  aux_1[mes-haceUnAnioMes].push(dato);
                }else if (dato.Title.indexOf("Biblioteca")!="-1") {
                  aux_2[mes-haceUnAnioMes].push(dato);
                }else if (dato.Title.indexOf("VT")!="-1") {
                  aux_3[mes-haceUnAnioMes].push(dato);
                }else if (dato.Title.indexOf("calendario eventos")!="-1") {
                  aux_4[mes-haceUnAnioMes].push(dato);
                }else if (dato.Title.indexOf("Generando conocimiento")!="-1") {
                  aux_5[mes-haceUnAnioMes].push(dato);
                }else if (dato.Title.indexOf("Portada")!="-1") {
                  aux_6[mes-haceUnAnioMes].push(dato);
                }else if (dato.Title.indexOf("informe vtgc")!="-1") {
                  aux_7[mes-haceUnAnioMes].push(dato);
                }
              }else{
                if(dato.Title.indexOf("Creatividad")!="-1"){
                  aux_1[11-(mesHoy-mes)].push(dato);
                }else if (dato.Title.indexOf("Biblioteca")!="-1") {
                  aux_2[11-(mesHoy-mes)].push(dato);
                }else if (dato.Title.indexOf("VT")!="-1") {
                  aux_3[11-(mesHoy-mes)].push(dato);
                }else if (dato.Title.indexOf("calendario eventos")!="-1") {
                  aux_4[11-(mesHoy-mes)].push(dato);
                }else if (dato.Title.indexOf("Generando conocimiento")!="-1") {
                  aux_5[11-(mesHoy-mes)].push(dato);
                }else if (dato.Title.indexOf("Portada")!="-1") {
                  aux_6[11-(mesHoy-mes)].push(dato);
                }else if (dato.Title.indexOf("informe vtgc")!="-1") {
                  aux_7[11-(mesHoy-mes)].push(dato);
                }
              }
            /*if(fechaComparar == labelsUltimos12Meses[j]){
              if(haceUnAnioValor == anioHoy){
                if(dato.Title.indexOf("Creatividad")!="-1"||dato.Title.indexOf("Lixiviados")!="-1"
                  ||dato.Title.indexOf("Vehículo eléctrico")!="-1"||dato.Title.indexOf("Recogida")!="-1"){
                  aux_1[mes].push(dato);
                }else if (dato.Title.indexOf("Biblioteca")!="-1") {
                  aux_2[mes].push(dato);
                }else if (dato.Title.indexOf("Historial VT")!="-1"||dato.Title.indexOf("Peticiones VT")!="-1"
                  ||dato.Title.indexOf("Historial BVT")!="-1") {
                  aux_3[mes].push(dato);
                }else if (dato.Title.indexOf("calendario eventos")!="-1") {
                  aux_4[mes].push(dato);
                }else if (dato.Title.indexOf("innomarket")!="-1"||dato.Title.indexOf("Generando conocimiento")!="-1"
                  ||dato.Title.indexOf("Innomarket")!="-1") {
                  aux_5[mes].push(dato);
                }else if (dato.Title.indexOf("Portada")!="-1") {
                  aux_6[mes].push(dato);
                }else if (dato.Title.indexOf("informe vtgc")!="-1") {
                  aux_7[mes].push(dato);
                }
              }else if(anio == haceUnAnioValor){
                if(dato.Title.indexOf("Creatividad")!="-1"||dato.Title.indexOf("Lixiviados")!="-1"
                  ||dato.Title.indexOf("Vehículo eléctrico")!="-1"||dato.Title.indexOf("Recogida")!="-1"){
                  aux_1[mes-haceUnAnioMes].push(dato);
                }else if (dato.Title.indexOf("Biblioteca")!="-1") {
                  aux_2[mes-haceUnAnioMes].push(dato);
                }else if (dato.Title.indexOf("Historial VT")!="-1"||dato.Title.indexOf("Peticiones VT")!="-1"
                  ||dato.Title.indexOf("Historial BVT")!="-1") {
                  aux_3[mes-haceUnAnioMes].push(dato);
                }else if (dato.Title.indexOf("calendario eventos")!="-1") {
                  aux_4[mes-haceUnAnioMes].push(dato);
                }else if (dato.Title.indexOf("innomarket")!="-1"||dato.Title.indexOf("Generando conocimiento")!="-1"
                  ||dato.Title.indexOf("Innomarket")!="-1") {
                  aux_5[mes-haceUnAnioMes].push(dato);
                }else if (dato.Title.indexOf("Portada")!="-1") {
                  aux_6[mes-haceUnAnioMes].push(dato);
                }else if (dato.Title.indexOf("informe vtgc")!="-1") {
                  aux_7[mes-haceUnAnioMes].push(dato);
                }
              }else{
                if(dato.Title.indexOf("Creatividad")!="-1"||dato.Title.indexOf("Lixiviados")!="-1"
                  ||dato.Title.indexOf("Vehículo eléctrico")!="-1"||dato.Title.indexOf("Recogida")!="-1"){
                  aux_1[11-(mesHoy-mes)].push(dato);
                }else if (dato.Title.indexOf("Biblioteca")!="-1") {
                  aux_2[11-(mesHoy-mes)].push(dato);
                }else if (dato.Title.indexOf("Historial VT")!="-1"||dato.Title.indexOf("Peticiones VT")!="-1"
                  ||dato.Title.indexOf("Historial BVT")!="-1") {
                  aux_3[11-(mesHoy-mes)].push(dato);
                }else if (dato.Title.indexOf("calendario eventos")!="-1") {
                  aux_4[11-(mesHoy-mes)].push(dato);
                }else if (dato.Title.indexOf("innomarket")!="-1"||dato.Title.indexOf("Generando conocimiento")!="-1"
                  ||dato.Title.indexOf("Innomarket")!="-1") {
                  aux_5[11-(mesHoy-mes)].push(dato);
                }else if (dato.Title.indexOf("Portada")!="-1") {
                  aux_6[11-(mesHoy-mes)].push(dato);
                }else if (dato.Title.indexOf("informe vtgc")!="-1") {
                  aux_7[11-(mesHoy-mes)].push(dato);
                }
              }*/
            }
          }

        }
      }

      $scope.actualizacionDatos = fechaMasReciente.getDate()+"/"+(fechaMasReciente.getMonth()+1)+"/"+fechaMasReciente.getFullYear();

      for (var i = 0; i < 12; i++) {
        if(aux_1[i] != null){
          var elemMasFrecuente = getMasFrecuente(aux_1[i]);
          if(elemMasFrecuente != null){
            datosMasAccedidosUltimos12Meses_1[i] = elemMasFrecuente[1];
            dataAux_1[i] = elemMasFrecuente[0];
          }
        }
        if(aux_2[i] != null){
          var elemMasFrecuente = getMasFrecuente(aux_2[i]);
          if(elemMasFrecuente != null){
            datosMasAccedidosUltimos12Meses_2[i] = elemMasFrecuente[1];
            dataAux_2[i] = elemMasFrecuente[0];
          }
        }
        if(aux_3[i] != null){
          var elemMasFrecuente = getMasFrecuente(aux_3[i]);
          if(elemMasFrecuente != null){
            datosMasAccedidosUltimos12Meses_3[i] = elemMasFrecuente[1];
            dataAux_3[i] = elemMasFrecuente[0];
          }
        }
        if(aux_4[i] != null){
          var elemMasFrecuente = getMasFrecuente(aux_4[i]);
          if(elemMasFrecuente != null){
            datosMasAccedidosUltimos12Meses_4[i] = elemMasFrecuente[1];
            dataAux_4[i] = elemMasFrecuente[0];
          }
        }
        if(aux_5[i] != null){
          var elemMasFrecuente = getMasFrecuente(aux_5[i]);
          if(elemMasFrecuente != null){
            datosMasAccedidosUltimos12Meses_5[i] = elemMasFrecuente[1];
            dataAux_5[i] = elemMasFrecuente[0];
          }
        }
        if(aux_6[i] != null){
          var elemMasFrecuente = getMasFrecuente(aux_6[i]);
          if(elemMasFrecuente != null){
            datosMasAccedidosUltimos12Meses_6[i] = elemMasFrecuente[1];
            dataAux_6[i] = elemMasFrecuente[0];
          }
        }
        if(aux_7[i] != null){
          var elemMasFrecuente = getMasFrecuente(aux_7[i]);
          if(elemMasFrecuente != null){
            datosMasAccedidosUltimos12Meses_7[i] = elemMasFrecuente[1];
            dataAux_7[i] = elemMasFrecuente[0];
          }
        }
        datosMasAccedidosUltimos12Meses_8[i] = datosMasAccedidosUltimos12Meses_1[i]+datosMasAccedidosUltimos12Meses_2[i]
        +datosMasAccedidosUltimos12Meses_3[i]+datosMasAccedidosUltimos12Meses_4[i]+datosMasAccedidosUltimos12Meses_5[i]
        +datosMasAccedidosUltimos12Meses_6[i]+datosMasAccedidosUltimos12Meses_7[i];
      }

      $scope.colores = ["#66FF66", "#FF6666", "#6666FF", "#ff66ff", "#ffb84d", "#e6e600", "#33ffff", "#bf8040"];
      $scope.series = ["Creatividad", "Bibliotecas", "VT", "Calendario eventos", "Conocimiento", "Portada",
        "Informe VTGC", "Indicadores totales"];

      $scope.data = [datosMasAccedidosUltimos12Meses_1, datosMasAccedidosUltimos12Meses_2, datosMasAccedidosUltimos12Meses_3,
        datosMasAccedidosUltimos12Meses_4, datosMasAccedidosUltimos12Meses_5, datosMasAccedidosUltimos12Meses_6,
        datosMasAccedidosUltimos12Meses_7, datosMasAccedidosUltimos12Meses_8];

      $scope.labels = labelsUltimos12Meses;
      $scope.options = {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero:true
            }
          }]
        },
        legend: {
          display: true
        },
        tooltips: {
          callbacks: {
            label: function(tooltipItem, data) {
              var index = tooltipItem.datasetIndex;
              if(index == 0){
                var num = data.datasets[index].data[tooltipItem.index];
                var elem = datosMasAccedidosUltimos12Meses_1[tooltipItem.index];
                var title = dataAux_1[tooltipItem.index].Title;
                if(elem > 0){
                  return " "+num+": "+title;
                }
              }else if(index == 1){
                var num = data.datasets[index].data[tooltipItem.index];
                var elem = datosMasAccedidosUltimos12Meses_2[tooltipItem.index];
                var title = dataAux_2[tooltipItem.index].Title;
                if(elem > 0){
                  return " "+num+": "+title;
                }
              }else if(index == 2){
                var num = data.datasets[index].data[tooltipItem.index];
                var elem = datosMasAccedidosUltimos12Meses_3[tooltipItem.index];
                var title = dataAux_3[tooltipItem.index].Title;
                if(elem > 0){
                  return " "+num+": "+title;
                }
              }else if(index == 3){
                var num = data.datasets[index].data[tooltipItem.index];
                var elem = datosMasAccedidosUltimos12Meses_4[tooltipItem.index];
                var title = dataAux_4[tooltipItem.index].Title;
                if(elem > 0){
                  return " "+num+": "+title;
                }
              }else if(index == 4){
                var num = data.datasets[index].data[tooltipItem.index];
                var elem = datosMasAccedidosUltimos12Meses_5[tooltipItem.index];
                var title = dataAux_5[tooltipItem.index].Title;
                if(elem > 0){
                  return " "+num+": "+title;
                }
              }else if(index == 5){
                var num = data.datasets[index].data[tooltipItem.index];
                var elem = datosMasAccedidosUltimos12Meses_6[tooltipItem.index];
                var title = dataAux_6[tooltipItem.index].Title;
                if(elem > 0){
                  return " "+num+": "+title;
                }
              }else if(index == 6){
                var num = data.datasets[index].data[tooltipItem.index];
                var elem = datosMasAccedidosUltimos12Meses_7[tooltipItem.index];
                var title = dataAux_7[tooltipItem.index].Title;
                if(elem > 0){
                  return " "+num+": "+title;
                }
              }else{
                var num = data.datasets[index].data[tooltipItem.index];
                return  " "+num+": Indicadores totales";
              }
            }
          }
        }
      };

      $scope.showSpinner = false;
      $timeout(function() {
        $scope.$apply();
      }, 100);

    });

    cargarIndicadoresService.getPersonas().then(function(data) {
      var datos = data.d.results;
      var departamentos = [];
      var departamentosAux = [];
      var departamentosFinal = [];
      var departamentosFinal2 = [];
      var labelsDepartamentos = [];
      var labelsDepartamentos2 = [];
      var colores = ["#b3d9ff"];
      var dep = ["", 0, 0];
      var index = 0;
      var index2 = -1;

      /*
      $scope.mostrarPersonas = [];
      var fecha = new Date("04/04/2017");
      for (var i = 0; i < datos.length; i++) {
        if (fecha < new Date(datos[i].Modified)) {
          $scope.mostrarPersonas.push(datos[i].Title+" ----- "+datos[i].Nombre.Department);
        }
      }*/

      for (var i = 0; i < datos.length; i++) {
      if(datos[i].Nombre.Department=='ADMINISTRACIÓN'){datos[i].Nombre.Department='Administración';
      }else if(datos[i].Nombre.Department=='CONSOLIDACION Y ADM. INTERNACIONAL'){datos[i].Nombre.Department='Administración';
      }else if(datos[i].Nombre.Department=='ADMINISTRACIÓN DE PERSONAL'){datos[i].Nombre.Department='Administración de personal';
      }else if(datos[i].Nombre.Department=='Central'){datos[i].Nombre.Department='Administración y Finanzas';
      }else if(datos[i].Nombre.Department=='Administración y Finanzas'){datos[i].Nombre.Department='Administración y Finanzas';
      }else if(datos[i].Nombre.Department=='CCR LA MULAS'){datos[i].Nombre.Department="Aguas y RCD's";
      }else if(datos[i].Nombre.Department=='Colunga-Villaviciosa'){datos[i].Nombre.Department="Aguas y RCD's";
      }else if(datos[i].Nombre.Department=='Dirección Construcción'){datos[i].Nombre.Department="Aguas y RCD's";
      }else if(datos[i].Nombre.Department=='Dirección Construcción'){datos[i].Nombre.Department="Aguas y RCD's";
      }else if(datos[i].Nombre.Department=='PT RCDs y Vertedero"La Salmedina"'){datos[i].Nombre.Department="Aguas y RCD's";
      }else if(datos[i].Nombre.Department=='ASESORÍA JURÍDICA'){datos[i].Nombre.Department='Asesoría jurídica';
      }else if(datos[i].Nombre.Department=='AUDITORÍAS ENERGÉTICAS'){datos[i].Nombre.Department='Auditoría energética';
      }else if(datos[i].Nombre.Department=='AUDITORÍA INTERNA'){datos[i].Nombre.Department='Auditoría interna';
      }else if(datos[i].Nombre.Department=='CALIDAD, PREVENCIÓN Y MEDIO AMBIENTE'){datos[i].Nombre.Department='CAPMA';
      }else if(datos[i].Nombre.Department=='PARQUE CENTRAL'){datos[i].Nombre.Department='CAPMA';
      }else if(datos[i].Nombre.Department=='CALIDAD'){datos[i].Nombre.Department='CAPMA';
      }else if(datos[i].Nombre.Department=='Dirección de Compras de Tratamiento'){datos[i].Nombre.Department='Compras de Tratamiento';
      }else if(datos[i].Nombre.Department=='Compras y Maquinaria'){datos[i].Nombre.Department='Compras y Maquinaria SU';
      }else if(datos[i].Nombre.Department=='CONTENCIOSO LABORAL'){datos[i].Nombre.Department='Contencioso Laboral';
      }else if(datos[i].Nombre.Department=='CONTRATACIÓN'){datos[i].Nombre.Department='Contratación';
      }else if(datos[i].Nombre.Department=='CONTROL DE GESTIÓN'){datos[i].Nombre.Department='Contratación';
      }else if(datos[i].Nombre.Department=='CUENTAS A COBRAR'){datos[i].Nombre.Department='Cuentas a Cobrar';
      }else if(datos[i].Nombre.Department=='DIR. GENERAL'){datos[i].Nombre.Department='Dirección General';
      }else if(datos[i].Nombre.Department=='Oman'){datos[i].Nombre.Department='Dirección País';
      }else if(datos[i].Nombre.Department=='Reino Unido'){datos[i].Nombre.Department='Dirección País';
      }else if(datos[i].Nombre.Department=='Dirección Recogida, Limpieza y Jardinería Internacional'){datos[i].Nombre.Department='Dirección Recogida, Limpieza y Jardinería Internacional';
      }else if(datos[i].Nombre.Department=='Dirección Recogida, Limpieza y Jardinería Nacional'){datos[i].Nombre.Department='Dirección Recogida, Limpieza y Jardinería Nacional';
      }else if(datos[i].Nombre.Department=='Dirección Tratamiento RSU Internacional'){datos[i].Nombre.Department='Dirección Tratamiento RSU Internacional';
      }else if(datos[i].Nombre.Department=='Dirección Tratamiento RSU Nacional'){datos[i].Nombre.Department='Dirección Tratamiento RSU Nacional';
      }else if(datos[i].Nombre.Department=='SIN PROYECTO'){datos[i].Nombre.Department='Dirección Zona Andalucía y Extremadura';
      }else if(datos[i].Nombre.Department=='D.Z. ANDALUCIA Y EXTREMADURA. REC. LIMP Y JARDINES'){datos[i].Nombre.Department='Dirección Zona Andalucía y Extremadura';
      }else if(datos[i].Nombre.Department=='DIREC. ZONA ARAGÓN, NAVARRA, PAIS VASCO, LA RIOJA Y CANTABRIA'){datos[i].Nombre.Department='Dirección Zona Aragón, Navarra, País Vasco, La Rioja y Cantabria';
      }else if(datos[i].Nombre.Department=='DIRECCIÓN ZONAL CANARIAS R, L Y J'){datos[i].Nombre.Department='Dirección Zona Canarias';
      }else if(datos[i].Nombre.Department=='D. Z. CASTILLA Y LEÓN, ASTURIAS Y GALICIA'){datos[i].Nombre.Department='Dirección Zona Castilla y León, Asturias y Galicia';
      }else if(datos[i].Nombre.Department=='DELEGACIÓN CATALUÑA TRATAMIENTO (GTOS. GENERALES)'){datos[i].Nombre.Department='Dirección Zona Cataluña y Baleares';
      }else if(datos[i].Nombre.Department=='COMPLEMENTARIO URBACET, S.L.'){datos[i].Nombre.Department='Dirección Zona Cataluña y Baleares';
      }else if(datos[i].Nombre.Department=='TALLER CATALUÑA'){datos[i].Nombre.Department='Dirección Zona Cataluña y Baleares';
      }else if(datos[i].Nombre.Department=='D.Z. LEVANTE Y CASTILLA LA MANCHA. REC, LIMP Y JARDINES'){datos[i].Nombre.Department='Dirección Zona Levante y Castilla La Mancha';
      }else if(datos[i].Nombre.Department=='D.Z. MADRID RECOGIDA, LIMPIEZA Y JARDINES'){datos[i].Nombre.Department='Dirección Zona Madrid';
      }else if(datos[i].Nombre.Department=='D.Z. MADRID  RECOGIDA, LIMPIEZA Y JARDINES'){datos[i].Nombre.Department='Dirección Zona Madrid';
      }else if(datos[i].Nombre.Department=='Emafesa'){datos[i].Nombre.Department='Emafesa';
      }else if(datos[i].Nombre.Department=='EMAFESA'){datos[i].Nombre.Department='Emafesa';
      }else if(datos[i].Nombre.Department=='Txorierri RSU'){datos[i].Nombre.Department='Enviser';
      }else if(datos[i].Nombre.Department=='Oficina Tec. Areas Verdes'){datos[i].Nombre.Department='Estudios de Jardinería';
      }else if(datos[i].Nombre.Department=='Oficina Tecnica SU y Areas Verdes Levante'){datos[i].Nombre.Department='Estudios de Jardinería';
      }else if(datos[i].Nombre.Department=='Oficina Tecnica SU'){datos[i].Nombre.Department='Estudios de Servicios Urbanos';
      }else if(datos[i].Nombre.Department=='Oficina Tecnica SU Asturias'){datos[i].Nombre.Department='Estudios de Servicios Urbanos';
      }else if(datos[i].Nombre.Department=='ARGANDA JARD.INTEGRAL'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='Chiclana Jardines'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='Comp. IGFA-URBASER UTE'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='CONCESIÓN DE ESPACIOS DEPORTIVOS VITORIA'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='Conserv. Y Limpieza Jardines Zona 2 Fuenlabrada'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='CONSERVACION VIAS Y ESPACIOS PUBLICOS LAS ROZAS LOTE 1'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='CONSERVACION VIAS Y ESPACIOS PUBLICOS LAS ROZAS LOTE 2'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='ECOPARC_2 Montcada y Rexac (2)'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='EXPLOTACION'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='EXPLOTACIÓN SU CATALUÑA'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='GENERALES'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='GESTION MEDIOAMBIENTAL TORRELAVEGA, S.A. (GESMATOR)'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='GOZON LIMPIEZA VIARIA'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='Jardineria Reus Zona Este'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='Leioa LV/RSU'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='LIMP. RECOG. DE FERROL'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='LIMP. RECOG. Y PTO. LIMPIO LOS ALCAZARES (NUEVO CONTRATO)'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='LIMP. URBANA DE CHICLANA DE LA FRONTERA'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='LIMP. Y RECOG. LA LAGUNA'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='Limp. y Recog. Navalmoral de la Mata'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='LIMP. Y RECOG. RSU MORÓN DE LA FR.'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='LIMP. Y RECOGIDA DE LUGO'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='Limp., Recog y Alcantarillado Mollet del Valles 2'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='LIMPIEZA CONTENEDORES MIJAS'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='LIMPIEZA PLAYAS DE ALMUÑÉCAR'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='Limpieza Puerto de Santander'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='LIMPIEZA SAN BARTOLOMÉ TIRAJ.'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='LIMPIEZA TORRES DE COTILLAS'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='Limpieza Viaria LINARES'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='Limpieza Viaria y Playas Ayamonte (Compl.)'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='LIMPIEZA VIARIA Y PLAYAS PUNTA UMBRIA'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='LIMPIEZA VIARIA Y PUNTO LIMPIO AMES'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='Limpieza y Mantenimiento Playas de Algeciras'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='LIMPIEZA Y RECOG. AVILES'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='LIMPIEZA Y RECOG. EN VILLALBA'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='LIMPIEZA Y RECOGIDA ARGANDA'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='LIMPIEZA Y RECOGIDA ARRECIFE'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='LIMPIEZA Y RECOGIDA CEUTA'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='LIMPIEZA Y RECOGIDA CORIA DEL RIO'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=="LIMPIEZA Y RECOGIDA DE L'ESCALA"){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='Limpieza y Recogida de Residuos Vallirana'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='Limpieza y Recogida Denia'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='LIMPIEZA Y RECOGIDA JÉREZ'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='LIMPIEZA Y RECOGIDA PRAT DE LLOBREGAT 2'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='LIMPIEZA Y RECOGIDA PTO. CÁDIZ'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='Limpieza y Recogida Roquetas'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='LIMPIEZA Y RECOGIDA RSU MUNICIPALES MARTORELL'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='LIMPIEZA Y RECOGIDA SANT JOAN DESPI'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='LIMPIEZA Y RECOGIDA SANTA CRUZ TENERIFE'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='LIMPIEZA Y RECOGIDA SANTIAGO DE COMPOSTELA'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='LIMPIEZA Y RECOGIDA SARIEGOS'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='LIMPIEZA Y RSU DE GRANOLLERS'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='LIMPIEZA Y RSU SANT ANDREU DE LA BARCA'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='Mant Vertedero y Planta de Lixiviados (TIRSSA)'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='MANTENIMIENTO'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='MANTENIMIENTO JARDINES SALOU'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='MANTENIMIENTO JARDINES SANTANDER'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='Mendizorrotza'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='MENSAJERIA Y LIMPIEZA'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='Mto Espacios Arco Medioambiental (Gijón)'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='Mto. Jardines Esplugues'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='Mto. Jardines Sta Cruz'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='Mto. Zonas verdes Lote 2 Dos Hermanas'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='Mto. Zonas Verdes Siero'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='P Envases y ET Comarca de Osona - VIC'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='PORT AVENTURA VIALES Y CAMPOS DE GOLF'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='Puntos Limpios Alcañiz'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='Rec. Selectiva Papel y Cartón Costa del Sol'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='RECOG. VARIOS MUN.PROVINC. BADAJOZ'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='RECOG. Y LIMP. 6 ZONAS NUEVAS ARROYOMOLINOS'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='RECOG. Y LV SAN MARTIN DE LA VEGA'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='RECOGIDA DE RIBEIRA'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='RECOGIDA REDONDELA'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='Recogida RSU 31 Municipios Badajoz'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='RECOGIDA RSU COMARCA RIBAGORZA'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='Recogida RSU y Limpieza Viaria Móstoles'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='Recogida RSU y Transp. Comarca Cinco Villas'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='Recogida Selectiva Costa del Sol'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='RECOGIDA Y LIMPIEZA ARANDA DE DUERO'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='Recogida y Limpieza de Calafell'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='RECOGIDA Y LIMPIEZA DE NARON'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='RECOGIDA Y LIMPIEZA DE VILASSAR DE MAR'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='RECOGIDA Y LIMPIEZA EN ELCHE'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='Recogida y Limpieza Mercado Central Madrid'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='RECOGIDA Y LIMPIEZA PALENCIA AYUNTAMIENTO'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='RECOGIDA Y LIMPIEZA VIARIA VILADECANS'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='RECOGIDA Y TRANSP CHICLANA (2)'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='RECOGIDA Y TTE RSU DE CONCA DE BARBERA'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='Recogida, Limpieza Viaria y Playas Torroella de Montgri'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='Recogida, Limpieza y Playas de Salou'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='Recogida, Limpieza y Playas de Vila Seca'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='Recogida, Transp. RSU y Envases (Córdoba)'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='RSU DE BARCELONA-Z.ESTE'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='RSU Y LIMPIEZA SAN FERNANDO'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='S.U. ARANJUEZ'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='S.U. ASTURIAS'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='S.U. BARCELONA'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='S.U. CAM Sur'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='S.U. DIRECCION DE ZONA'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='S.U.Extremadura'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='Santoña LV y R'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='SELECTIVA Y PUNTO LIMPIO SAN FERNANDO'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='Selur'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='Selur Servicio de Limpieza Urgente_2 (SELUR)'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='SERV. LIMP. Y RECOG. ALMONTE'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='SERVICIOS FERROL'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='TARRAGONA'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='TIRSSA'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='Trab. Jardinería Área Metropolitana Barcelona'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='Trabajos Camion Aspirador'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='URBASER TRANSPORTES'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='UTE COSTA BALLENA (COMPLEMENTARIO)'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='UTE DEPENDENCIAS PUERTO DE PALMA II COMPLEMENTARIO'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='UTE ESPACIOS VERDES BILBAO'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='Ute Esplugues II'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='Ute Fuentes de Zaragoza Complementario'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='UTE LIMPIEZA CUENCA'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='UTE MOSTOLES ZONA SUR II'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='UTE RECOGIDA PERIFERIA MADRID'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='UTE RECOGIDA SUR MADRID'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='UTE Recogida Sur Madrid_COMP'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='ET y Vertedero Arico'){datos[i].Nombre.Department='Explotación Tratamiento';
      }else if(datos[i].Nombre.Department=='CASARES'){datos[i].Nombre.Department='Explotación Tratamiento';
      }else if(datos[i].Nombre.Department=='Comp. PTRSU Cuenca'){datos[i].Nombre.Department='Explotación Tratamiento';
      }else if(datos[i].Nombre.Department=='COMP. PTRSU Elche'){datos[i].Nombre.Department='Explotación Tratamiento';
      }else if(datos[i].Nombre.Department=='Comp. PTRSU Pinto'){datos[i].Nombre.Department='Explotación Tratamiento';
      }else if(datos[i].Nombre.Department=='Comp. Zonas VI, VII, IX - LLiria y Caudete'){datos[i].Nombre.Department='Explotación Tratamiento';
      }else if(datos[i].Nombre.Department=='Comp.PTRSU Leon'){datos[i].Nombre.Department='Explotación Tratamiento';
      }else if(datos[i].Nombre.Department=='ECOPARC_2 Montcada y Rexac (2)'){datos[i].Nombre.Department='Explotación Tratamiento';
      }else if(datos[i].Nombre.Department=='ECOPARQUE NAVALMORAL DE LA MATA'){datos[i].Nombre.Department='Explotación Tratamiento';
      }else if(datos[i].Nombre.Department=='ECOPARQUE VILLANUEVA DE LA SERENA'){datos[i].Nombre.Department='Explotación Tratamiento';
      }else if(datos[i].Nombre.Department=='Explotación tratamiento'){datos[i].Nombre.Department='Explotación Tratamiento';
      }else if(datos[i].Nombre.Department=='Explotación Tratamiento'){datos[i].Nombre.Department='Explotación Tratamiento';
      }else if(datos[i].Nombre.Department=='GASTOS GENERALES'){datos[i].Nombre.Department='Explotación Tratamiento';
      }else if(datos[i].Nombre.Department=='PLANTA DE BIOMETANIZACION'){datos[i].Nombre.Department='Explotación Tratamiento';
      }else if(datos[i].Nombre.Department=='PLANTA DE CAUDETE'){datos[i].Nombre.Department='Explotación Tratamiento';
      }else if(datos[i].Nombre.Department=='PLANTA DE ENVASES PTO DE SANTA MARIA'){datos[i].Nombre.Department='Explotación Tratamiento';
      }else if(datos[i].Nombre.Department=='PTIRSU LAS LOMAS'){datos[i].Nombre.Department='Explotación Tratamiento';
      }else if(datos[i].Nombre.Department=='PTIRSU Meruelo'){datos[i].Nombre.Department='Explotación Tratamiento';
      }else if(datos[i].Nombre.Department=='PTRSU "CAMPIÑA 2000"'){datos[i].Nombre.Department='Explotación Tratamiento';
      }else if(datos[i].Nombre.Department=='PTRSU "Las Dehesas"'){datos[i].Nombre.Department='Explotación Tratamiento';
      }else if(datos[i].Nombre.Department=='PTRSU AVILA NORTE II'){datos[i].Nombre.Department='Explotación Tratamiento';
      }else if(datos[i].Nombre.Department=='PTRSU BIO-Pinto'){datos[i].Nombre.Department='Explotación Tratamiento';
      }else if(datos[i].Nombre.Department=='PTRSU BIO-PINTO'){datos[i].Nombre.Department='Explotación Tratamiento';
      }else if(datos[i].Nombre.Department=='PTRSU COSTA DEL SOL'){datos[i].Nombre.Department='Explotación Tratamiento';
      }else if(datos[i].Nombre.Department=='PTRSU Cuenca'){datos[i].Nombre.Department='Explotación Tratamiento';
      }else if(datos[i].Nombre.Department=='PTRSU Nostian'){datos[i].Nombre.Department='Explotación Tratamiento';
      }else if(datos[i].Nombre.Department=='PTRSU Sur de Europa'){datos[i].Nombre.Department='Explotación Tratamiento';
      }else if(datos[i].Nombre.Department=='PTRSU Zamora'){datos[i].Nombre.Department='Explotación Tratamiento';
      }else if(datos[i].Nombre.Department=='PTRSU Zaragoza'){datos[i].Nombre.Department='Explotación Tratamiento';
      }else if(datos[i].Nombre.Department=='RESUR JAEN'){datos[i].Nombre.Department='Explotación Tratamiento';
      }else if(datos[i].Nombre.Department=='TIRMADRID'){datos[i].Nombre.Department='Explotación Tratamiento';
      }else if(datos[i].Nombre.Department=='Total Ecoparc 1 - Zona Franca'){datos[i].Nombre.Department='Explotación Tratamiento';
      }else if(datos[i].Nombre.Department=='TRAT. PLANTA CANTABRIA'){datos[i].Nombre.Department='Explotación Tratamiento';
      }else if(datos[i].Nombre.Department=='Trat. Y  Eliminación de Residuos Urbanos Madrid Zona Norte'){datos[i].Nombre.Department='Explotación Tratamiento';
      }else if(datos[i].Nombre.Department=='TRATAMIENTO INTEGRAL DE RESIDUOS ZONZAMAS, S.A.'){datos[i].Nombre.Department='Explotación Tratamiento';
      }else if(datos[i].Nombre.Department=='Tratamientos Cataluña'){datos[i].Nombre.Department='Explotación Tratamiento';
      }else if(datos[i].Nombre.Department=='UTE LOGROÑO'){datos[i].Nombre.Department='Explotación Tratamiento';
      }else if(datos[i].Nombre.Department=='UTRERA RESIDUOS'){datos[i].Nombre.Department='Explotación Tratamiento';
      }else if(datos[i].Nombre.Department=='FINANZAS'){datos[i].Nombre.Department='Finanzas';
      }else if(datos[i].Nombre.Department=='Dir. Gestíón de Energía'){datos[i].Nombre.Department='Gestión de Energía';
      }else if(datos[i].Nombre.Department=='Dirección de Ingeniería de Tratamiento'){datos[i].Nombre.Department='Ingeniería de Tratamiento';
      }else if(datos[i].Nombre.Department=='INNOVACIÓN'){datos[i].Nombre.Department='Innovación';
      }else if(datos[i].Nombre.Department=='DIRECCIÓN DE INNOVACIÓN'){datos[i].Nombre.Department='Innovación';
      }else if(datos[i].Nombre.Department=='Dirección Internacional'){datos[i].Nombre.Department='Internacional';
      }else if(datos[i].Nombre.Department=='Oficina Técnica Tratamiento'){datos[i].Nombre.Department='Ofertas Tratamiento';
      }else if(datos[i].Nombre.Department=='Dir. Procedimientos Negociados'){datos[i].Nombre.Department='Procedimientos negociados';
      }else if(datos[i].Nombre.Department=='Dir. Proyectos Tratamiento'){datos[i].Nombre.Department='Proyectos Tratamiento';
      }else if(datos[i].Nombre.Department=='RELACIONES LABORALES'){datos[i].Nombre.Department='Relaciones Laborales';
      }else if(datos[i].Nombre.Department=='SELECCIÓN Y FORMACIÓN'){datos[i].Nombre.Department='Relaciones Laborales';
      }else if(datos[i].Nombre.Department=='SEGUROS'){datos[i].Nombre.Department='Seguros';
      }else if(datos[i].Nombre.Department=='ACEITES'){datos[i].Nombre.Department='Sertego';
      }else if(datos[i].Nombre.Department=='ALFARO'){datos[i].Nombre.Department='Sertego';
      }else if(datos[i].Nombre.Department=='ANDALUCIA MARPOLES'){datos[i].Nombre.Department='Sertego';
      }else if(datos[i].Nombre.Department=='Central'){datos[i].Nombre.Department='Sertego';
      }else if(datos[i].Nombre.Department=='CENTRO DE TRANSFERENCIA'){datos[i].Nombre.Department='Sertego';
      }else if(datos[i].Nombre.Department=='Cetransa'){datos[i].Nombre.Department='Sertego';
      }else if(datos[i].Nombre.Department=='CETRANSA'){datos[i].Nombre.Department='Sertego';
      }else if(datos[i].Nombre.Department=='COMERCIAL'){datos[i].Nombre.Department='Sertego';
      }else if(datos[i].Nombre.Department=='COMUNIDAD DE MADRID ACEITE'){datos[i].Nombre.Department='Sertego';
      }else if(datos[i].Nombre.Department=='COMUNIDAD DE MADRID RESIDUOS'){datos[i].Nombre.Department='Sertego';
      }else if(datos[i].Nombre.Department=='Dirección Industrial'){datos[i].Nombre.Department='Sertego';
      }else if(datos[i].Nombre.Department=='FUENLABRADA REGENERACION'){datos[i].Nombre.Department='Sertego';
      }else if(datos[i].Nombre.Department=='GALICIA RESIDUOS'){datos[i].Nombre.Department='Sertego';
      }else if(datos[i].Nombre.Department=='LA RIOJA  RESIDUOS'){datos[i].Nombre.Department='Sertego';
      }else if(datos[i].Nombre.Department=='PAIS VASCO ENVASES'){datos[i].Nombre.Department='Sertego';
      }else if(datos[i].Nombre.Department=='PALMA MARPOLES'){datos[i].Nombre.Department='Sertego';
      }else if(datos[i].Nombre.Department=='PALMA RESIDUOS'){datos[i].Nombre.Department='Sertego';
      }else if(datos[i].Nombre.Department=='PALOS DE LA FRONTERA REGENERACION'){datos[i].Nombre.Department='Sertego';
      }else if(datos[i].Nombre.Department=='RESIDUOS INDUSTRIALES ZARAGOZA (RINZA, S.A.)'){datos[i].Nombre.Department='Sertego';
      }else if(datos[i].Nombre.Department=='Rinza'){datos[i].Nombre.Department='Sertego';
      }else if(datos[i].Nombre.Department=='SANTA FE ACEITE'){datos[i].Nombre.Department='Sertego';
      }else if(datos[i].Nombre.Department=='Sertego Tanger_Comp'){datos[i].Nombre.Department='Sertego';
      }else if(datos[i].Nombre.Department=='TRELIMSA (TRESIMA LIMPIEZAS INDUSTRIALES, S.A.)'){datos[i].Nombre.Department='Sertego';
      }else if(datos[i].Nombre.Department=='URBAMAR RI MARPOLES'){datos[i].Nombre.Department='Sertego';
      }else if(datos[i].Nombre.Department=='URBAMAR RI RESIDUOS'){datos[i].Nombre.Department='Sertego';
      }else if(datos[i].Nombre.Department=='UTE SERTEGO Y EONA BIOMASA  COMPLEMENTARIO'){datos[i].Nombre.Department='Sertego';
      }else if(datos[i].Nombre.Department=='SERVICIOS GENERALES'){datos[i].Nombre.Department='Servicios Generales';
      }else if(datos[i].Nombre.Department=='SISTEMAS DE INFORMACIÓN'){datos[i].Nombre.Department='Sistemas de Información';
      }else if(datos[i].Nombre.Department=='Sistemas de Información'){datos[i].Nombre.Department='Sistemas de Información';
      }else if(datos[i].Nombre.Department=='Oficina Técnica'){datos[i].Nombre.Department='Oficina Técnica Socamex';
      }else if(datos[i].Nombre.Department=='Oficina Técnica - Estructura'){datos[i].Nombre.Department='Oficina Técnica Socamex';
      }else if(datos[i].Nombre.Department=='ABAS. AGUAS. MUNIC. SARIEGOS'){datos[i].Nombre.Department='Socamex';
      }else if(datos[i].Nombre.Department=='Central'){datos[i].Nombre.Department='Socamex';
      }else if(datos[i].Nombre.Department=='COMPRAS'){datos[i].Nombre.Department='Socamex';
      }else if(datos[i].Nombre.Department=='Contadores Hispanagua'){datos[i].Nombre.Department='Socamex';
      }else if(datos[i].Nombre.Department=='Direccion'){datos[i].Nombre.Department='Socamex';
      }else if(datos[i].Nombre.Department=='EDAR SUR ORIENTAL COMPLEMENTARIO'){datos[i].Nombre.Department='Socamex';
      }else if(datos[i].Nombre.Department=='EDARS MARE ZONA 1'){datos[i].Nombre.Department='Socamex';
      }else if(datos[i].Nombre.Department=='G.I.A. ALHAMA'){datos[i].Nombre.Department='Socamex';
      }else if(datos[i].Nombre.Department=='Innovación y Desarrollo Aguas'){datos[i].Nombre.Department='Socamex';
      }else if(datos[i].Nombre.Department=='Oficina Técnica Socamex'){datos[i].Nombre.Department='Socamex';
      }else if(datos[i].Nombre.Department=='REPSOL MEDITERRÁNEO'){datos[i].Nombre.Department='Socamex';
      }else if(datos[i].Nombre.Department=='SERV.AGUAS,ALCANT. Y DEPURACIÓN EN ALMENDRALEJO'){datos[i].Nombre.Department='Socamex';
      }else if(datos[i].Nombre.Department=='UTE AGUAS DE NAVALMORAL DE LA MATA'){datos[i].Nombre.Department='Socamex';
      }else if(datos[i].Nombre.Department=='UTE Alcantarillado Sevilla'){datos[i].Nombre.Department='Socamex';
      }else if(datos[i].Nombre.Department=='CONTRATACIÓN'){datos[i].Nombre.Department='Contratación';
      }else if(datos[i].Nombre.Department=='ABAS. AGUAS. MUNIC. SARIEGOS'){datos[i].Nombre.Department='Socamex';
      }else if(datos[i].Nombre.Department=='ADMINISTRACIÓN'){datos[i].Nombre.Department='Administración';
      }else if(datos[i].Nombre.Department=='ALFARO ACEITE'){datos[i].Nombre.Department='Sertego';
      }else if(datos[i].Nombre.Department=='ASESORÍA JURÍDICA'){datos[i].Nombre.Department='Asesoría Jurídica';
      }else if(datos[i].Nombre.Department=='Bahrain'){datos[i].Nombre.Department='Dirección País';
      }else if(datos[i].Nombre.Department=='CALIDAD, PREVENCIÓN Y MEDIO AMBIENTE'){datos[i].Nombre.Department='CAPMA';
      }else if(datos[i].Nombre.Department=='CARTAGENA REGENERACION'){datos[i].Nombre.Department='Sertego';
      }else if(datos[i].Nombre.Department=='Central'){datos[i].Nombre.Department='Administración';
      }else if(datos[i].Nombre.Department=='Chiclana Jardines'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='Compras y Maquinaria'){datos[i].Nombre.Department='Compras y Maquinaria';
      }else if(datos[i].Nombre.Department=='DELEGACIÓN CATALUÑA TRATAMIENTO (GTOS. GENERALES)'){datos[i].Nombre.Department='Dirección Zona Cataluña y Baleares';
      }else if(datos[i].Nombre.Department=='Dir. Procedimientos Negociados'){datos[i].Nombre.Department='Procedimientos negociados';
      }else if(datos[i].Nombre.Department=='Dir. Proyectos Tratamiento'){datos[i].Nombre.Department='Proyectos Tratamiento';
      }else if(datos[i].Nombre.Department=='ECOPARQUE NAVALMORAL DE LA MATA'){datos[i].Nombre.Department='Explotación Tratamiento';
      }else if(datos[i].Nombre.Department=='ECOPARQUE VILLANUEVA DE LA SERENA'){datos[i].Nombre.Department='Explotación Tratamiento';
      }else if(datos[i].Nombre.Department=='Estructura'){datos[i].Nombre.Department='Enviser';
      }else if(datos[i].Nombre.Department=='ET y Vertedero Arico'){datos[i].Nombre.Department='Explotación Tratamiento';
      }else if(datos[i].Nombre.Department=='EXPL. PUNTOS LIMPIOS G.CANARIA'){datos[i].Nombre.Department='Explotación Tratamiento';
      }else if(datos[i].Nombre.Department=='FUENLABRADA REGENERACION'){datos[i].Nombre.Department='Sertego';
      }else if(datos[i].Nombre.Department=='LA RIOJA RESIDUOS'){datos[i].Nombre.Department='Sertego';
      }else if(datos[i].Nombre.Department=='Limp. y Recog. Navalmoral de la Mata'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='LIMP. Y RECOGIDA DE LUGO'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='LIMP. Y RECOGIDA DE NOVELDA'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='LIMPIEZA CONTENEDORES MIJAS'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='Limpieza y Recogida de Fuengirola'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='LIMPIEZA Y RECOGIDA SANTA CRUZ TENERIFE'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='MANTENIMIENTO JARDINES SANTANDER'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='MENSAJERIA Y LIMPIEZA'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='Mto. Zonas Verdes Tarragona'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='Oficina Tec. Areas Verdes'){datos[i].Nombre.Department='Estudios de Jardinería';
      }else if(datos[i].Nombre.Department=='Oficina Técnica'){datos[i].Nombre.Department='Socamex';
      }else if(datos[i].Nombre.Department=='Oficina Tecnica SU'){datos[i].Nombre.Department='Estudios de Servicios Urbanos';
      }else if(datos[i].Nombre.Department=='PLANTA DE ENVASES PTO DE SANTA MARIA'){datos[i].Nombre.Department='Explotación Tratamiento';
      }else if(datos[i].Nombre.Department=='PT RCDs y Vertedero"La Salmedina"'){datos[i].Nombre.Department='Explotación Tratamiento';
      }else if(datos[i].Nombre.Department=='PTIRSU Meruelo'){datos[i].Nombre.Department='Explotación Tratamiento';
      }else if(datos[i].Nombre.Department=='PTRSU "CAMPIÑA 2000"'){datos[i].Nombre.Department='Explotación Tratamiento';
      }else if(datos[i].Nombre.Department=='PTRSU AVILA NORTE II'){datos[i].Nombre.Department='Explotación Tratamiento';
      }else if(datos[i].Nombre.Department=='Puntos Limpios Alcañiz'){datos[i].Nombre.Department='Explotación Tratamiento';
      }else if(datos[i].Nombre.Department=='RECOG. Y LV SAN MARTIN DE LA VEGA'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='RECOGIDA Y LIMPIEZA ARANDA DE DUERO'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='Recogida y Limpieza de Calafell'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='RECOGIDA Y LIMPIEZA EN ELCHE'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='RECOGIDA Y LIMPIEZA PALENCIA AYUNTAMIENTO'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='RECOGIDA Y LIMPIEZA SAN ADRIA DE BESOS'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='RECOGIDA Y LIMPIEZA VIARIA VILADECANS'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='RECOGIDA Y TRANSP CHICLANA (2)'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='Recogida, Limpieza y Playas de Vila Seca'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='RELACIONES LABORALES'){datos[i].Nombre.Department='Relaciones Laborales';
      }else if(datos[i].Nombre.Department=='RELACIONES LABORALES'){datos[i].Nombre.Department='Relaciones Laborales';
      }else if(datos[i].Nombre.Department=='RSU DE BARCELONA'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='SELECCIÓN Y FORMACIÓN'){datos[i].Nombre.Department='Selección y Formación';
      }else if(datos[i].Nombre.Department=='SERVICIOS GENERALES'){datos[i].Nombre.Department='Servicios Generales';
      }else if(datos[i].Nombre.Department=='Sistemas de Información'){datos[i].Nombre.Department='Sistemas de Información';
      }else if(datos[i].Nombre.Department=='SUMIN. AGUAS ISLA MAYOR (VILLAFRANCO GUADALQUIVIR)'){datos[i].Nombre.Department='Socamex';
      }else if(datos[i].Nombre.Department=='Total Ecoparc 1'){datos[i].Nombre.Department='Explotación Tratamiento';
      }else if(datos[i].Nombre.Department=='Total Ecoparc 1'){datos[i].Nombre.Department='Explotación Tratamiento';
      }else if(datos[i].Nombre.Department=='URBAMAR RI MARPOLES'){datos[i].Nombre.Department='Sertego';
      }else if(datos[i].Nombre.Department=='UTE EBRO'){datos[i].Nombre.Department='Dirección Zona Aragón, Navarra, País Vasco, La Rioja y Cantabria';
      }else if(datos[i].Nombre.Department=='UTE MELILLA COMPLEMENTARIO'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='UTE MOSTOLES ZONA SUR II'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='UTE Recogida Sur Madrid_COMP'){datos[i].Nombre.Department='Explotación SU';
      }else if(datos[i].Nombre.Department=='UTE TENERIFE'){datos[i].Nombre.Department='Explotación Tratamiento';
      }else if(datos[i].Nombre.Department=='UTE VALDEMINGOMEZ 2000'){datos[i].Nombre.Department='Explotación Tratamiento';
      }else if(datos[i].Nombre.Department=='UTRERA RESIDUOS'){datos[i].Nombre.Department='Sertego';
      }else if(datos[i].Nombre.Department=='ZAMORA, VERTEDERO'){datos[i].Nombre.Department='Explotación Tratamiento';
      }else{
        datos[i].Nombre.Department=null;
      }
      }

      //Creamos un array de departamentos y lo ordenamos alfabeticamente para tener elementos iguales juntos
      for (var i = 0; i < datos.length; i++) {
        if(datos[i].Nombre.Department != null){
          departamentos.push(datos[i].Nombre.Department);
        }
      }

      departamentos.sort();

      //Creamos un array auxiliar para contar el numero de apariciones de cada departamento y
      //lo ordenamos de menor a mayor por frecuencia de aparicion
      for (var i = 0; i < departamentos.length; i++) {
        if(dep[0] != departamentos[i]){
          dep = [departamentos[i], index, 1];
          departamentosAux.push(dep);
          index2++;
        }else{
          departamentosAux[index2][2]++;
        }
        index++;
      }

      departamentosAux.sort(customOrder);

      //Creamos el array definitivo
      for (var i = 0; i < departamentosAux.length; i++) {
        if(departamentosAux[i][2] > 0){
          departamentosFinal.push(departamentosAux[i][2]);
          labelsDepartamentos.push(departamentosAux[i][0]);
        }
      }

      $scope.labels2 = labelsDepartamentos;
      $scope.data2 = departamentosFinal;

      $scope.labels3 = labelsDepartamentos2;
      $scope.data3 = departamentosFinal2;

      $scope.showSpinner2 = false;
      $timeout(function() {
        $scope.$apply();
      }, 100);

    });
  });
