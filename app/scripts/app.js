'use strict';

/**
 * @ngdoc overview
 * @name indicadoresApp
 * @description
 * # indicadoresApp
 *
 * Main module of the application.
 */
angular
  .module('indicadoresApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'chart.js',
    'angularSpinner'
  ])
  .config(function ($routeProvider, $locationProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/creatividad', {
        templateUrl: 'views/creatividad.html',
        controller: 'CreatividadCtrl',
        controllerAs: 'creatividad'
      })
      .when('/vt', {
        templateUrl: 'views/vt.html',
        controller: 'VtCtrl',
        controllerAs: 'vt'
      })
      .when('/calendario', {
        templateUrl: 'views/calendario.html',
        controller: 'CalendarioCtrl',
        controllerAs: 'calendario'
      })
      .when('/conocimiento', {
        templateUrl: 'views/conocimiento.html',
        controller: 'ConocimientoCtrl',
        controllerAs: 'conocimiento'
      })
      .when('/bibliotecas', {
        templateUrl: 'views/bibliotecas.html',
        controller: 'BibliotecasCtrl',
        controllerAs: 'bibliotecas'
      })
      .otherwise({
        redirectTo: '/'
      });
      $locationProvider.hashPrefix('');
  });
